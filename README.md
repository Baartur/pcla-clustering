# PCLA-Clustering

A comparison of several NLP approaches to BODACC PCLA clustering. 

## Project instructions : 

Instructions can be found [here](https://docs.google.com/document/d/1aELRgn_ktwdeV-r4dZtvLypcVHG15ux0UjPIF_8E2ZE/edit). 

## Project description : 

1. In France, the administration must guarantee transparency in the economic and financial sphere. Therefore, the services produce an official journal called BODACC (it stands for “Bulletin officiel des annonces civiles et commerciales”). Business information about firms from founding to deregistration is recorded in this bulletin. It includes declarations of a sale, companies’ transfers, bankruptcy proceedings, filing of annual accounts, … In this project, we focus our attention on bankruptcy proceedings (“procédures collectives” in French also denoted by PCLA). The objective is to group the PCLAs by type in order to know the exact nature of the difficulties faced by the companies.​​​​​

1. ​To achieve this task, we will use a dataset containing PCLA from 2017 to 2019. This dataset represents more than 500 000 court rulings. It is available on the following GitLab repository:  https://gitlab.com/Baartur/pcla-clustering. Our dataset contains 3 levels of information for each PCLA. From the least precise to the most precise level, we have its family (e.g. "Jugement de clôture "), its nature (e.g. "Jugement de clôture pour insuffisance d'actif"), and its complement (e.g. "Jugement prononçant la clôture du plan de cession pour insuffisance d'actif "). The complement to the judgment is exhaustive and sometimes takes up two or even three sentences. We will therefore try to categorize the PCLAs by using their judgment complement. The families and natures of judgments will be useful for (1) defining the target of our supervised sequence classification models, and (2) quantitatively evaluating our unsupervised clustering models.​

1. ​We identify two main families of NLP models, the classical models that we oppose to the deep learning models. In our answer to the problem of categorizing PCLAs, we will try to compare these two approaches to highlight their advantages and disadvantages, and finally select the most relevant one for our use case. The LDA model will be our benchmark model. LDA is indeed a classical topic modeling approach, widely used, and relatively interpretable.  However, LDA remains a "bag of words" approach, not deducing any meaning from the arrangement of words in sentences. To remedy the performance limitation of this type of modeling, we will test more complex models based on deep learning methods.

1.  ​Firstly, we will realize a topic modeling with Gensim’s LDA in order to understand the different themes contained by the documents. To visualize the different topic clusters PyLDAvis will be useful.

1.  ​Then, we will compare this benchmark model to one or several deep learning models. This model could be unsupervised or supervised, using the jugement family as a target or even by designing a manual labeling process that would feed the target. We could use methods that will directly perform topic modeling task, or we can couple an embedding layer with a numerical clustering layer (e.g. models for these 2 possibilities: Word2Vec, CAMENBERT, Sentence-BERT, LSTM, FastText, GPT2 combined with LGBM, KMEANS, ANN). We will test it qualitatively using dimensionality reduction techniques (like PCA, TSNE). We will then evaluate it quantitatively using different scores (like proportion scores) computed on the test set of the source dataset. 

1.  ​We will then draw conclusions and possible future work. 

## Project report : 

A report of this project will be available before May 24th 8pm. 

Authors can edit the report by clicking [here](https://www.overleaf.com/project/606c3fe95895d327882b59e9)
